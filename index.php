<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Bebrand");
?>
<!-- header -->
<header id="header" class="container-fluid header">
    <!-- header__top -->
    <div class="header__top">
        <div class="container header__top-container">
            <div class="header__text">
	            <?$APPLICATION->IncludeFile('/include/header_top.html')?>
            </div>
            <button class="navbar-toggler header__collapse" type="button"></button>
        </div>
    </div><!-- header__top END -->
    <!-- header__block -->
    <div class="container header__block">
        <!-- header__navbar -->
        <nav class="navbar navbar-expand-sm header__navbar">
            <a class="navbar-brand logo" href=""><img src="<?=SITE_TEMPLATE_PATH?>/img/header/header_logo.png" alt="" class="logo__img"></a>
            <!-- header__nav -->
            <div class="navbar-collapse header__nav" id="header__nav">
                <!-- topnav -->
                <ul class="navbar-nav topnav">
                    <li class="nav-item topnav__item">
                        <a class="nav-link topnav__link" href="#services"><span class="topnav__link-border">Услуги</span></a>
                    </li>
                    <li class="nav-item topnav__item">
                        <a class="nav-link topnav__link" href="#map"><span class="topnav__link-border">Контакты</span></a>
                    </li>
                    <li class="nav-item topnav__item">
                        <a class="nav-link topnav__link" href="#about"><span class="topnav__link-border">О компании</span></a>
                    </li>
                    <li class="nav-item topnav__item">
                        <a class="nav-link topnav__link" href="#question"><span class="topnav__link-border">Вопрос-ответ</span></a>
                    </li>
                    <li class="nav-item topnav__item">
                        <a class="nav-link topnav__link" href="#videoreview"><span class="topnav__link-border">Отзывы</span></a>
                    </li>
                </ul><!-- topnav END -->
            </div><!-- header__nav END -->
            <!-- freecheck -->
            <div class="freecheck">
                <button class="btn freecheck__btn">Бесплатная проверка</button>
                <div class="freecheck__phone">
                    <?$APPLICATION->IncludeFile('/include/header_phone.html')?>
                </div>
            </div><!-- freecheck END -->
        </nav><!-- header__navbar END -->
        <!-- header__body -->
        <div class="header__body">
            <!-- header__leftside -->
            <div class="header__leftside">
                <h1 class="header__maintitle">
                    <?$APPLICATION->IncludeFile('/include/header_title.html')?>
                </h1>
                <!-- toplist -->
	            <?$APPLICATION->IncludeFile('/include/header_list.html')?>
                <!-- toplist END -->
                <button class="btn header__btn">Запросить бесплатную консультацию</button>
            </div><!-- header__leftside END -->
            <!-- header__rightside -->
            <div class="header__rightside">
                <!-- header__video -->
                <div class="embed-responsive embed-responsive-16by9 header__video">
	                <?$APPLICATION->IncludeFile('/include/header_video_iframe.html')?>
                </div><!-- header__video END -->
            </div><!-- header__rightside END -->
        </div><!-- header__body END -->
    </div><!-- header__block END -->
    <!-- header__scroll -->
    <a href="#about" class="header__scroll"></a><!-- header__scroll END -->
</header><!-- header END -->
<!-- about -->
<section id="about" class="container-fluid about">
    <div class="container about__block">
        <h2 class="section__title about__maintitle">
            <span class="about__maintitle-bg">
                <?$APPLICATION->IncludeFile('/include/about_title.html')?>
            </span>
        </h2>
        <div class="about__body">
            <div class="about__text">
	            <?$APPLICATION->IncludeFile('/include/about_text.html')?>
            </div>
        </div>
    </div>
</section><!-- about END-->
<!-- howitworks -->
<section id="howitworks" class="container-fluid howitworks">
    <!-- howitworks__block -->
    <div class="container howitworks__block">
        <h2 class="section__title howitworks__maintitle">
	        <?$APPLICATION->IncludeFile('/include/about_how_it_works.html')?>
        </h2>
	    <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "how_it_works",
            array(
                "COMPONENT_TEMPLATE" => "how_it_works",
                "IBLOCK_TYPE" => "content",
                "IBLOCK_ID" => "1",
                "NEWS_COUNT" => "10",
                "SORT_BY1" => "SORT",
                "SORT_ORDER1" => "ASC",
                "SORT_BY2" => "ID",
                "SORT_ORDER2" => "ASC",
                "FILTER_NAME" => "",
                "FIELD_CODE" => array(
                    0 => "NAME",
                    1 => "PREVIEW_TEXT",
                    2 => "",
                ),
                "PROPERTY_CODE" => array(
                    0 => "",
                    1 => "",
                ),
                "CHECK_DATES" => "Y",
                "DETAIL_URL" => "",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "N",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "CACHE_TYPE" => "A",
                "CACHE_TIME" => "3600",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "N",
                "PREVIEW_TRUNCATE_LEN" => "",
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "SET_TITLE" => "N",
                "SET_BROWSER_TITLE" => "N",
                "SET_META_KEYWORDS" => "N",
                "SET_META_DESCRIPTION" => "N",
                "SET_STATUS_404" => "N",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                "ADD_SECTIONS_CHAIN" => "N",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "INCLUDE_SUBSECTIONS" => "Y",
                "DISPLAY_DATE" => "N",
                "DISPLAY_NAME" => "N",
                "DISPLAY_PICTURE" => "N",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "PAGER_TEMPLATE" => ".default",
                "DISPLAY_TOP_PAGER" => "N",
                "DISPLAY_BOTTOM_PAGER" => "N",
                "PAGER_TITLE" => "Новости",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "SET_LAST_MODIFIED" => "N",
                "STRICT_SECTION_CHECK" => "N",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "SHOW_404" => "N",
                "MESSAGE_404" => ""
            ), false
        );?>
	    <?$APPLICATION->IncludeComponent(
		    "bitrix:news.list",
		    "how_it_works_slider",
		    array(
			    "COMPONENT_TEMPLATE" => "how_it_works_slider",
			    "IBLOCK_TYPE" => "content",
			    "IBLOCK_ID" => "1",
			    "NEWS_COUNT" => "10",
			    "SORT_BY1" => "SORT",
			    "SORT_ORDER1" => "ASC",
			    "SORT_BY2" => "ID",
			    "SORT_ORDER2" => "ASC",
			    "FILTER_NAME" => "",
			    "FIELD_CODE" => array(
				    0 => "NAME",
				    1 => "PREVIEW_TEXT",
				    2 => "",
			    ),
			    "PROPERTY_CODE" => array(
				    0 => "",
				    1 => "",
			    ),
			    "CHECK_DATES" => "Y",
			    "DETAIL_URL" => "",
			    "AJAX_MODE" => "N",
			    "AJAX_OPTION_JUMP" => "N",
			    "AJAX_OPTION_STYLE" => "N",
			    "AJAX_OPTION_HISTORY" => "N",
			    "AJAX_OPTION_ADDITIONAL" => "",
			    "CACHE_TYPE" => "A",
			    "CACHE_TIME" => "3600",
			    "CACHE_FILTER" => "N",
			    "CACHE_GROUPS" => "N",
			    "PREVIEW_TRUNCATE_LEN" => "",
			    "ACTIVE_DATE_FORMAT" => "d.m.Y",
			    "SET_TITLE" => "N",
			    "SET_BROWSER_TITLE" => "N",
			    "SET_META_KEYWORDS" => "N",
			    "SET_META_DESCRIPTION" => "N",
			    "SET_STATUS_404" => "N",
			    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			    "ADD_SECTIONS_CHAIN" => "N",
			    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
			    "PARENT_SECTION" => "",
			    "PARENT_SECTION_CODE" => "",
			    "INCLUDE_SUBSECTIONS" => "Y",
			    "DISPLAY_DATE" => "N",
			    "DISPLAY_NAME" => "N",
			    "DISPLAY_PICTURE" => "N",
			    "DISPLAY_PREVIEW_TEXT" => "Y",
			    "PAGER_TEMPLATE" => ".default",
			    "DISPLAY_TOP_PAGER" => "N",
			    "DISPLAY_BOTTOM_PAGER" => "N",
			    "PAGER_TITLE" => "Новости",
			    "PAGER_SHOW_ALWAYS" => "N",
			    "PAGER_DESC_NUMBERING" => "N",
			    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			    "PAGER_SHOW_ALL" => "N",
			    "SET_LAST_MODIFIED" => "N",
			    "STRICT_SECTION_CHECK" => "N",
			    "PAGER_BASE_LINK_ENABLE" => "N",
			    "SHOW_404" => "N",
			    "MESSAGE_404" => ""
		    ), false
	    );?>
        <button id="how-it-works-button" class="btn howitworks__btn">Заказать регистрацию товарного знака</button>
    </div>
    <!-- howitworks__block END -->
</section>
<!-- howitworks END -->
<?
$types = [];
$property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 7, "CODE" => "TYPE"));
while($enum_fields = $property_enums->GetNext()) {
	$types[Cutil::translit($enum_fields['VALUE'], 'ru', [])] = $enum_fields['VALUE'];
}

$cities = [];
$property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 7, "CODE" => "CITY"));
while($enum_fields = $property_enums->GetNext()) {
	$cities[Cutil::translit($enum_fields['VALUE'], 'ru', [])] = $enum_fields['VALUE'];
}
?>
<!-- ourclients -->
<section id="ourclients" class="container-fluid ourclients">
    <!-- ourclients__block -->
    <div class="container ourclients__block">
        <!-- ourclients__top -->
        <div class="ourclients__top">
            <? foreach ($types as $key => $type) { ?>
                <h3 class="ourclients__title active" data-filter="<?=$key?>"><?=$type?></h3>
            <? } ?>
        </div>
        <!-- ourclients__top END -->
        <!-- ourclients__body -->
        <div class="ourclients__body">
            <!-- ourclients__leftside -->
            <div class="ourclients__leftside">
                <? foreach ($cities as $key => $city) { ?>
                    <a class="ourclients__links active" data-filter="<?=$key?>"><span class="ourclients__links-border"><?=$city?></span></a>
                <? } ?>
            </div>
            <!-- ourclients__leftside END -->
	        <?
	        $brands = [];
	        $arSelect = [
                "ID",
                "NAME",
                "PROPERTY_CITY",
                "PROPERTY_TYPE",
                "PREVIEW_PICTURE",
                "PROPERTY_TRADE_MARK",
                "PROPERTY_FILE_TEXT",
                "PROPERTY_FILE",
            ];
	        $arFilter = Array("IBLOCK_ID" => 7, "ACTIVE"=>"Y");
	        $res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
	        while($ob = $res->GetNextElement()) {
	            $fields = $ob->GetFields();
		        $picture = CFile::ResizeImageGet($fields['PREVIEW_PICTURE'], array('width'=>170, 'height'=>70), BX_RESIZE_IMAGE_PROPORTIONAL, true);
		        $fields['PREVIEW_PICTURE'] = $picture['src'];
		        $fields['FILE'] = CFile::GetPath($fields["PROPERTY_FILE_VALUE"]);
		        $brands[] = $fields;
	        }
	        ?>
            <!-- clientslider -->
            <div class="clientslider owl-carousel owl-theme">
                <? foreach ($brands as $brand) { ?>
	                <?
                    $city_code = Cutil::translit($brand['PROPERTY_CITY_VALUE'], 'ru', []);
                    $type_code = Cutil::translit($brand['PROPERTY_TYPE_VALUE'], 'ru', []);
                    ?>
                    <!-- clientslider__item -->
                    <div class="clientslider__item <?=$city_code?> <?=$type_code?>">
                        <!-- cards -->
                        <div class="cards">
                            <!-- cards__top -->
                            <div class="cards__top">
                                <img src="<?=$brand['PREVIEW_PICTURE']?>" alt="<?=$brand['NAME']?>" title="<?=$brand['NAME']?>" class="cards__img">
                                <p class="cards__text">
                                    <? if ($brand["PROPERTY_TRADE_MARK_VALUE"] != '') {
                                        echo $brand["PROPERTY_TRADE_MARK_VALUE"];
                                    } else {
	                                    echo 'Торговая марка';
                                    } ?>
                                </p>
                                <h4 class="cards__title"><?=$brand['NAME']?></h4>
                            </div>
                            <!-- cards__top END -->
                            <? if ($brand['FILE']) { ?>
                                <a href="<?=$brand['FILE']?>" target="_blank" class="cards__link">
                                    <? if ($brand["PROPERTY_FILE_TEXT_VALUE"] != '') {
                                        echo $brand["PROPERTY_FILE_TEXT_VALUE"];
                                    } else {
                                        echo 'Посмотреть<br>свидетельство';
                                    } ?>
                                </a>
                            <? } ?>
                        </div>
                        <!-- cards END -->
                    </div>
                    <!-- clientslider__item END -->
                <? } ?>
                <!-- clientslider__item -->
            </div>
            <!-- clientslider END -->
        </div>
        <!-- ourclients__body END -->
        <div class="ourclients__bottom">
            <div class="slideresult">
                <span class="slideresult__data" data-filter="abakan">
                    <?$APPLICATION->IncludeFile('/include/slideresult_abakan.html')?>
                </span>
                <span class="slideresult__data" data-filter="krasnoyarsk">
                    <?$APPLICATION->IncludeFile('/include/slideresult_krasnoyarsk.html')?>
                </span>
                <span class="slideresult__data" data-filter="rossiya">
                    <?$APPLICATION->IncludeFile('/include/slideresult_rossiya.html')?>
                </span>
                 /
                <span class="slideresult__link">Абакан</span>
            </div>
            <div class="ourclients__controls">
                <i class="controls__left"></i>
                <i class="controls__right"></i>
            </div>
        </div>
    </div><!-- ourclients__block END -->
</section><!-- ourclients END -->
<!-- registration -->
<section id="registration" class="container-fluid registration">
    <!-- registration__block -->
    <div class="container registration__block">
        <!-- registration__top -->
        <div class="registration__top">
            <h2 class="registration__maintitle">
                <span class="highlight">
                    <?$APPLICATION->IncludeFile('/include/rights_title.html')?>
                </span>
            </h2>
        </div><!-- registration__top END -->
        <!-- registration__body -->
        <div class="registration__body">
            <!-- registration__item -->
            <div class="registration__item">
                <div class="registration__title">
                    <span class="highlight">
                        <?$APPLICATION->IncludeFile('/include/rights_subtitle_1.html')?>
                    </span>
                </div>
                <br>
                <div class="registration__text">
                    <span class="highlight">
                        <?$APPLICATION->IncludeFile('/include/rights_text_1.html')?>
                    </span>
                </div>
            </div><!-- registration__item END -->
            <!-- registration__item -->
            <div class="registration__item">
                <div class="registration__title">
                    <span class="highlight">
                        <?$APPLICATION->IncludeFile('/include/rights_subtitle_2.html')?>
                    </span>
                </div>
                <br>
                <div class="registration__text">
                    <span class="highlight">
                        <?$APPLICATION->IncludeFile('/include/rights_text_2.html')?>
                    </span>
                </div>
            </div><!-- registration__item END -->
            <!-- registration__item -->
            <div class="registration__item">
                <div class="registration__title">
                    <span class="highlight">
                        <?$APPLICATION->IncludeFile('/include/rights_subtitle_3.html')?>
                    </span>
                </div>
                <br>
                <div class="registration__text">
                    <span class="highlight">
                        <?$APPLICATION->IncludeFile('/include/rights_text_3.html')?>
                    </span>
                </div>
            </div><!-- registration__item END -->
        </div><!-- registration__body END -->
        <button class="btn registration__btn">Оформить товарный знак</button>
    </div><!-- registration__block END -->
</section><!-- registration END -->
<!-- services -->
<section id="services" class="container-fluid services">
    <!-- services__block -->
    <div class="container services__block">
        <h2 class="section__title services__maintitle">
	        <?$APPLICATION->IncludeFile('/include/services_title.html')?>
        </h2>
	    <?$APPLICATION->IncludeComponent(
		    "bitrix:news.list",
		    "services",
		    array(
			    "COMPONENT_TEMPLATE" => "services",
			    "IBLOCK_TYPE" => "content",
			    "IBLOCK_ID" => "2",
			    "NEWS_COUNT" => "7",
			    "SORT_BY1" => "SORT",
			    "SORT_ORDER1" => "ASC",
			    "SORT_BY2" => "ID",
			    "SORT_ORDER2" => "ASC",
			    "FILTER_NAME" => "",
			    "FIELD_CODE" => array(
				    0 => "NAME",
				    1 => "PREVIEW_TEXT",
				    2 => "",
			    ),
			    "PROPERTY_CODE" => array(
				    0 => "",
				    1 => "",
			    ),
			    "CHECK_DATES" => "Y",
			    "DETAIL_URL" => "",
			    "AJAX_MODE" => "N",
			    "AJAX_OPTION_JUMP" => "N",
			    "AJAX_OPTION_STYLE" => "N",
			    "AJAX_OPTION_HISTORY" => "N",
			    "AJAX_OPTION_ADDITIONAL" => "",
			    "CACHE_TYPE" => "A",
			    "CACHE_TIME" => "3600",
			    "CACHE_FILTER" => "N",
			    "CACHE_GROUPS" => "N",
			    "PREVIEW_TRUNCATE_LEN" => "",
			    "ACTIVE_DATE_FORMAT" => "d.m.Y",
			    "SET_TITLE" => "N",
			    "SET_BROWSER_TITLE" => "N",
			    "SET_META_KEYWORDS" => "N",
			    "SET_META_DESCRIPTION" => "N",
			    "SET_STATUS_404" => "N",
			    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			    "ADD_SECTIONS_CHAIN" => "N",
			    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
			    "PARENT_SECTION" => "",
			    "PARENT_SECTION_CODE" => "",
			    "INCLUDE_SUBSECTIONS" => "Y",
			    "DISPLAY_DATE" => "N",
			    "DISPLAY_NAME" => "N",
			    "DISPLAY_PICTURE" => "N",
			    "DISPLAY_PREVIEW_TEXT" => "Y",
			    "PAGER_TEMPLATE" => ".default",
			    "DISPLAY_TOP_PAGER" => "N",
			    "DISPLAY_BOTTOM_PAGER" => "N",
			    "PAGER_TITLE" => "Новости",
			    "PAGER_SHOW_ALWAYS" => "N",
			    "PAGER_DESC_NUMBERING" => "N",
			    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			    "PAGER_SHOW_ALL" => "N",
			    "SET_LAST_MODIFIED" => "N",
			    "STRICT_SECTION_CHECK" => "N",
			    "PAGER_BASE_LINK_ENABLE" => "N",
			    "SHOW_404" => "N",
			    "MESSAGE_404" => ""
		    ), false
	    );?>
    </div><!-- services__block END -->
</section><!-- services END -->
<!-- reason -->
<section id="reason" class="container-fluid reason">
    <!-- reason__block -->
    <div class="container reason__block">
        <h2 class="section__title reason__maintitle">
            <?$APPLICATION->IncludeFile('/include/why_title.html')?>
        </h2>
        <!-- reason__galerry -->
        <div class="reason__galerry">
            <!-- reason__col -->
            <div class="reason__col">
                <!-- reason__item -->
                <div class="reason__item">
                    <p class="reason__number">1</p>
                    <p class="reason__text">
	                    <?$APPLICATION->IncludeFile('/include/why_text_1.html')?>
                    </p>
                </div><!-- reason__item END -->
            </div><!-- reason__col END -->
            <!-- reason__col -->
            <div class="reason__col">
                <!-- reason__item -->
                <div class="reason__item">
                    <p class="reason__number">2</p>
                    <p class="reason__text">
	                    <?$APPLICATION->IncludeFile('/include/why_text_2.html')?>
                    </p>
                </div><!-- reason__item END -->
            </div><!-- reason__col END -->
            <!-- reason__col -->
            <div class="reason__col">
                <!-- reason__item -->
                <div class="reason__item">
                    <p class="reason__number">3</p>
                    <p class="reason__text">
	                    <?$APPLICATION->IncludeFile('/include/why_text_3.html')?>
                    </p>
                </div><!-- reason__item END -->
            </div><!-- reason__col END -->
        </div><!-- reason__galerry END -->
    </div><!-- reason__block END -->
</section><!-- reason END -->
<!-- modify -->
<section id="modify" class="container-fluid modify">
    <div class="container modify__block">
        <h2 class="section__title modify__maintitle">
	        <?$APPLICATION->IncludeFile('/include/how_title.html')?>
        </h2>
        <div class="embed-responsive embed-responsive-16by9 modify__video">
	        <?$APPLICATION->IncludeFile('/include/how_video_iframe.html')?>
        </div>
    </div>
</section>
<!-- modify END -->
<!-- geography -->
<?include 'include/geography.php'?>
<!-- geography END -->
<!-- videoreview -->
<section id="videoreview" class="container-fluid videoreview">
    <!-- videoreview__block -->
    <div class="container videoreview__block">
        <!-- videoreview__leftside -->
        <div class="videoreview__leftside">
            <h2 class="videoreview__maintitle">
                <?$APPLICATION->IncludeFile('/include/videoreview_title.html')?>
            </h2>
            <?
            $cities = [];
            $property_enums = CIBlockPropertyEnum::GetList(array("DEF" => "DESC", "SORT" => "ASC"), array("IBLOCK_ID" => 6, "CODE" => "CITY"));
            while($enum_fields = $property_enums->GetNext()) {
                $cities[Cutil::translit($enum_fields['VALUE'], 'ru', [])] = $enum_fields['VALUE'];
            }
            ?>
            <!-- videoreview__links -->
            <div class="videoreview__links">
                <? foreach ($cities as $key => $city) { ?>
                    <a class="videoreview__link active" data-filter="<?=$key?>">
                        <span class="videoreview__link-border"><?=$city?></span>
                    </a>
                <? } ?>
            </div>
            <!-- videoreview__links END -->
        </div>
        <!-- videoreview__leftside END -->
        <!-- videoreview__rightside -->
        <div class="videoreview__rightside">
            <p class="videoreview__count">
                <?$APPLICATION->IncludeFile('/include/videoreview_count.html')?>
            </p>
            <?
            $videos = [];
            $arSelect = Array("ID", "PROPERTY_CITY", "PROPERTY_VIDEO");
            $arFilter = Array("IBLOCK_ID" => 6, "ACTIVE"=>"Y");
            $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
            while($ob = $res->GetNextElement()) {
	            $videos[] = $ob->GetFields();
            }
            ?>
            <!-- reviewslider -->
            <div class="reviewslider owl-carousel owl-theme">
                <? foreach ($videos as $video) { ?>
                    <?$city_code = Cutil::translit($video['PROPERTY_CITY_VALUE'], 'ru', []);?>
                    <div class="embed-responsive embed-responsive-16by9 reviewslider__item">
                        <iframe class="embed-responsive-item <?=$city_code?> filter" src="<?=$video['PROPERTY_VIDEO_VALUE']?>"></iframe>
                    </div>
                <? } ?>
            </div>
            <!-- reviewslider END -->
            <!-- videoreview__controls -->
            <div class="videoreview__controls">
                <div class="reviewslider__dots"></div>
                <div class="reviewslider__controls">
                    <i class="controls__left controls__left-black"></i>
                    <i class="controls__right controls__right-black"></i>
                </div>
            </div>
            <!-- videoreview__controls END -->
        </div>
        <!-- videoreview__rightside END -->
    </div>
    <!-- videoreview__block END -->
</section>
<!-- videoreview END -->
<!-- question -->
<section id="question" class="container-fluid question">
    <!-- question__block -->
    <div class="container question__block">
        <h2 class="section__title question__maintitle">
            <?$APPLICATION->IncludeFile('/include/faq_title.html')?>
        </h2>
        <!-- question__body -->
        <div class="question__body">
            <!-- question__leftside -->
            <div class="question__leftside">
	            <?$APPLICATION->IncludeComponent(
		            "bitrix:news.list",
		            "faq",
		            array(
			            "COMPONENT_TEMPLATE" => "faq",
			            "IBLOCK_TYPE" => "content",
			            "IBLOCK_ID" => "4",
			            "NEWS_COUNT" => "5",
			            "SORT_BY1" => "SORT",
			            "SORT_ORDER1" => "ASC",
			            "SORT_BY2" => "ID",
			            "SORT_ORDER2" => "ASC",
			            "FILTER_NAME" => "",
			            "FIELD_CODE" => array(
				            0 => "NAME",
				            1 => "PREVIEW_TEXT",
				            2 => "",
			            ),
			            "PROPERTY_CODE" => array(
				            0 => "",
				            1 => "",
			            ),
			            "CHECK_DATES" => "Y",
			            "DETAIL_URL" => "",
			            "AJAX_MODE" => "N",
			            "AJAX_OPTION_JUMP" => "N",
			            "AJAX_OPTION_STYLE" => "N",
			            "AJAX_OPTION_HISTORY" => "N",
			            "AJAX_OPTION_ADDITIONAL" => "",
			            "CACHE_TYPE" => "A",
			            "CACHE_TIME" => "3600",
			            "CACHE_FILTER" => "N",
			            "CACHE_GROUPS" => "N",
			            "PREVIEW_TRUNCATE_LEN" => "",
			            "ACTIVE_DATE_FORMAT" => "d.m.Y",
			            "SET_TITLE" => "N",
			            "SET_BROWSER_TITLE" => "N",
			            "SET_META_KEYWORDS" => "N",
			            "SET_META_DESCRIPTION" => "N",
			            "SET_STATUS_404" => "N",
			            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			            "ADD_SECTIONS_CHAIN" => "N",
			            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
			            "PARENT_SECTION" => "",
			            "PARENT_SECTION_CODE" => "",
			            "INCLUDE_SUBSECTIONS" => "Y",
			            "DISPLAY_DATE" => "N",
			            "DISPLAY_NAME" => "N",
			            "DISPLAY_PICTURE" => "N",
			            "DISPLAY_PREVIEW_TEXT" => "Y",
			            "PAGER_TEMPLATE" => ".default",
			            "DISPLAY_TOP_PAGER" => "N",
			            "DISPLAY_BOTTOM_PAGER" => "N",
			            "PAGER_TITLE" => "Новости",
			            "PAGER_SHOW_ALWAYS" => "N",
			            "PAGER_DESC_NUMBERING" => "N",
			            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			            "PAGER_SHOW_ALL" => "N",
			            "SET_LAST_MODIFIED" => "N",
			            "STRICT_SECTION_CHECK" => "N",
			            "PAGER_BASE_LINK_ENABLE" => "N",
			            "SHOW_404" => "N",
			            "MESSAGE_404" => ""
		            ), false
	            );?>
            </div>
            <!-- question__leftside END -->
            <!-- questionform-->
            <div class="questionform">
                <h2 class="section__title questionform__title">
	                <?$APPLICATION->IncludeFile('/include/faq_have_questions.html')?>
                </h2>
                <!-- questionform__forms -->
                <form class="questionform__forms" method="post" action="/ajax/questions.php">
                    <input required name="name" type="text" class="form-control questionform__input questionform__input-name" placeholder="Введите Ваше имя">
                    <input required name="phone" type="text" class="form-control questionform__input questionform__input-phone input__inputmaskJS" placeholder="Введите телефон">
                    <textarea name="question" class="form-control questionform__input questionform__input-comment" rows="3" id="comment" placeholder="Ваш вопрос"></textarea>
                    <input type="submit" class="btn howitworks__btn howitworks__btn-questionform" value="Задать вопрос">
                </form>
                <!-- questionform__forms END -->
            </div>
            <!-- questionform END -->
        </div>
        <!-- question__body END -->
        <!-- question__bottom -->
        <div class="question__bottom">
            <p class="question__text question__text-bottom">
                <?$APPLICATION->IncludeFile('/include/faq_text.html')?>
            </p>
        </div>
        <!-- question__bottom END -->
    </div>
    <!-- question__block END -->
</section>
<!-- question END -->
<!-- sertificate -->
<section id="sertificate" class="container-fluid sertificate">
    <!-- sertificate__block -->
    <div class="container sertificate__block">
        <h2 class="section__title sertificate__maintitle">
            <?$APPLICATION->IncludeFile('/include/certificate_title.html')?>
        </h2>
	    <?$APPLICATION->IncludeComponent(
		    "bitrix:news.list",
		    "certificates",
		    array(
			    "COMPONENT_TEMPLATE" => "certificates",
			    "IBLOCK_TYPE" => "content",
			    "IBLOCK_ID" => "3",
			    "NEWS_COUNT" => "5",
			    "SORT_BY1" => "SORT",
			    "SORT_ORDER1" => "ASC",
			    "SORT_BY2" => "ID",
			    "SORT_ORDER2" => "ASC",
			    "FILTER_NAME" => "",
			    "FIELD_CODE" => array(
				    0 => "NAME",
				    1 => "PREVIEW_TEXT",
				    2 => "",
			    ),
			    "PROPERTY_CODE" => array(
				    0 => "",
				    1 => "",
			    ),
			    "CHECK_DATES" => "Y",
			    "DETAIL_URL" => "",
			    "AJAX_MODE" => "N",
			    "AJAX_OPTION_JUMP" => "N",
			    "AJAX_OPTION_STYLE" => "N",
			    "AJAX_OPTION_HISTORY" => "N",
			    "AJAX_OPTION_ADDITIONAL" => "",
			    "CACHE_TYPE" => "A",
			    "CACHE_TIME" => "3600",
			    "CACHE_FILTER" => "N",
			    "CACHE_GROUPS" => "N",
			    "PREVIEW_TRUNCATE_LEN" => "",
			    "ACTIVE_DATE_FORMAT" => "d.m.Y",
			    "SET_TITLE" => "N",
			    "SET_BROWSER_TITLE" => "N",
			    "SET_META_KEYWORDS" => "N",
			    "SET_META_DESCRIPTION" => "N",
			    "SET_STATUS_404" => "N",
			    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			    "ADD_SECTIONS_CHAIN" => "N",
			    "HIDE_LINK_WHEN_NO_DETAIL" => "N",
			    "PARENT_SECTION" => "",
			    "PARENT_SECTION_CODE" => "",
			    "INCLUDE_SUBSECTIONS" => "Y",
			    "DISPLAY_DATE" => "N",
			    "DISPLAY_NAME" => "N",
			    "DISPLAY_PICTURE" => "N",
			    "DISPLAY_PREVIEW_TEXT" => "Y",
			    "PAGER_TEMPLATE" => ".default",
			    "DISPLAY_TOP_PAGER" => "N",
			    "DISPLAY_BOTTOM_PAGER" => "N",
			    "PAGER_TITLE" => "Новости",
			    "PAGER_SHOW_ALWAYS" => "N",
			    "PAGER_DESC_NUMBERING" => "N",
			    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			    "PAGER_SHOW_ALL" => "N",
			    "SET_LAST_MODIFIED" => "N",
			    "STRICT_SECTION_CHECK" => "N",
			    "PAGER_BASE_LINK_ENABLE" => "N",
			    "SHOW_404" => "N",
			    "MESSAGE_404" => ""
		    ), false
	    );?>
    </div>
    <!-- sertificate__block END -->
</section>
<!-- sertificate END -->
<section id="map" class="container-fluid map">
    <div class="container map__block">
        <div class="map__top">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/map/logo.png" alt="" class="map__logo">
            <div class="map__contacts">
	            <?$APPLICATION->IncludeFile('/include/map_contacts.html')?>
            </div>
	        <?$APPLICATION->IncludeFile('/include/map_phone.html')?>
        </div>
    </div>
    <div id="map__body" class="map__body"></div>
</section>
<!-- footer -->
<footer id="footer" class="container-fluid footer">
    <!-- footer__block -->
    <div class="container footer__block">
        <!-- footer__brand -->
        <div class="footer__brand">
            <img src="<?=SITE_TEMPLATE_PATH?>/img/footer/footer_logo.png" alt="" class="footer__img">
            <div class="footer__text">
                <?$APPLICATION->IncludeFile('/include/footer_description.html')?>
            </div>
        </div><!-- footer__brand END -->
        <!-- footer__details -->
        <div class="footer__details">
            <?$APPLICATION->IncludeFile('/include/footer_contacts.html')?>
        </div><!-- footer__details END -->
        <!-- callback -->
        <div class="callback">
	        <?$APPLICATION->IncludeFile('/include/footer_phone.html')?>
            <p class="callback__text">Заказать обратный звонок</p>
            <div class="footer__text footer__text-callback">
                <?$APPLICATION->IncludeFile('/include/footer_callback.html')?>
            </div>
        </div><!-- callback END -->
    </div><!-- footer__block END -->
</footer><!-- footer END -->
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>