<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="overlay"></div>
<div class="call-popup">
    <span class="call-popup__close"></span>
    <div class="call-popup__title">Заявка на звонок</div>
    <form class="call-popup__form" method="post" action="/ajax/call.php">
        <input required name="name" type="text" class="form-control call-popup__input" placeholder="Введите Ваше имя">
        <input required name="phone" type="text" class="form-control call-popup__input input__inputmaskJS" placeholder="Введите телефоне">
        <input type="submit" class="btn howitworks__btn call-popup__btn" value="Задать вопрос">
    </form>
</div>
<script>
    window.onload = function () {
        if (screen.width <= 480) {
            document.getElementById('vp').setAttribute('content', 'width=480, initial-scale=1, maximum-scale=1, shrink-to-fit=no');
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApRkTxTtGOfSZzgQK7rTFQenSx_n5uNkg&callback=initMap"></script>
</body>
</html>