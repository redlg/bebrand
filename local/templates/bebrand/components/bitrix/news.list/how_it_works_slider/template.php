<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- howitworks__slider -->
<div class="howitworks__slider">
    <i class="howitworks__control howitworks__control-prev"></i>
    <!-- stageslider -->
    <div class="stageslider owl-carousel owl-theme">
        <? foreach($arResult["ITEMS"] as $arItem) { ?>
            <!-- stages__item -->
            <div class="stages__item">
                <div class="stages__top stages__top-lupa"></div>
                <h3 class="stages__title"><?echo $arItem["NAME"]?></h3>
                <p class="stages__text"><?echo $arItem["PREVIEW_TEXT"];?></p>
            </div>
            <!-- stages__item END -->
        <? } ?>
    </div>
    <!-- stageslider END -->
    <i class="howitworks__control howitworks__control-next"></i>
</div>
<!-- howitworks__slider END -->
