<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- services__gallery -->
<div class="services__gallery">
    <? foreach($arResult["ITEMS"] as $arItem) { ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <!-- services__col -->
        <div class="services__col" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <!-- services__item -->
            <div class="services__item">
                <!-- services__thumbnail -->
                <div class="services__thumbnail">
                    <img src="<?=$arItem['PREVIEW_PICTURE']['src']?>" alt="<?=$arItem["NAME"]?>" title="<?=$arItem["NAME"]?>" class="services__img">
                    <h3 class="services__title"><?echo $arItem["NAME"]?></h3>
                </div>
                <!-- services__thumbnail END -->
            </div>
            <!-- services__item END -->
        </div>
        <!-- services__col END -->
    <? } ?>
    <div class="services__col">
        <!-- services__item -->
        <div class="services__item">
            <!-- services__thumbnail -->
            <div class="services__thumbnail">
                <img src="<?=SITE_TEMPLATE_PATH?>/img/services/services__img-4.png" alt="" class="services__img">
                <button class="btn services__btn">Заказать проверку</button>
            </div><!-- services__thumbnail END -->
        </div><!-- services__item END -->
    </div><!-- services__col END -->
</div>
<!-- services__gallery END -->
