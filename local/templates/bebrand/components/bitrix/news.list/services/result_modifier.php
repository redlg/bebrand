<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['ITEMS'] as $key => $arItem) {
	$arResult['ITEMS'][$key]['PREVIEW_PICTURE'] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>248, 'height'=>248), BX_RESIZE_IMAGE_PROPORTIONAL, true);
}