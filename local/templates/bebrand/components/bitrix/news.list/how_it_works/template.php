<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- stages -->
<div class="stages">
    <? foreach($arResult["ITEMS"] as $arItem) { ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <!-- stages__item -->
        <div class="stages__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="stages__top stages__top-lupa"></div>
            <h3 class="stages__title"><?echo $arItem["NAME"]?></h3>
            <p class="stages__text"><?echo $arItem["PREVIEW_TEXT"];?></p>
        </div>
        <!-- stages__item END -->
    <? } ?>
</div>
<!-- stage END -->
