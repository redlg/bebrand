<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? foreach($arResult["ITEMS"] as $arItem) { ?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <!-- question__tab -->
	<div class="question__tab" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
        <h3 class="question__title">
            <span class="question__title-border">
                <?echo $arItem["NAME"]?>
            </span>
        </h3>
        <!-- question__tabsbody -->
        <div class="question__tabsbody">
            <p class="question__text">
	            <?echo $arItem["PREVIEW_TEXT"];?>
            </p>
        </div>
        <!-- question__tabsbody END -->
	</div>
    <!-- question__tab END -->
<? } ?>