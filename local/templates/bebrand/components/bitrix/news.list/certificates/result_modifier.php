<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

foreach ($arResult['ITEMS'] as $key => $arItem) {
	$arResult['ITEMS'][$key]['THUMB_PICTURE'] = CFile::ResizeImageGet($arItem['PREVIEW_PICTURE'], array('width'=>100, 'height'=>140), BX_RESIZE_IMAGE_PROPORTIONAL, true);
}