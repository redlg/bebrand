<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<!-- sertificate__gallery -->
<div class="sertificate__gallery">
    <? foreach($arResult["ITEMS"] as $arItem) { ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <!-- sertificate__item -->
        <a href="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" data-fancybox class="sertificate__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <img src="<?=$arItem['THUMB_PICTURE']['src']?>" alt="<?=$arItem['NAME']?>" title="<?=$arItem['NAME']?>" class="sertificate__img">
        </a>
        <!-- sertificate__item END -->
    <? } ?>
</div>
<!-- sertificate__gallery END -->
