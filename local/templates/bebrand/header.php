<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<html lang="<?=LANGUAGE_ID?>">
<head>
	<?$APPLICATION->ShowHead();?>
    <meta charset="utf-8">
    <title><?$APPLICATION->ShowTitle();?></title>
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta id="vp" name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <meta property="og:image" content="path/to/image.jpg">
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=SITE_TEMPLATE_PATH?>/img/favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=SITE_TEMPLATE_PATH?>/img/favicon/apple-touch-icon-114x114.png">
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/fonts.min.css")?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/main.min.css")?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/redline.css")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/libs.min.js")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/common.js")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/redline.js")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/map.js")?>
</head>
<body>
<div id="panel"><?$APPLICATION->ShowPanel();?></div>