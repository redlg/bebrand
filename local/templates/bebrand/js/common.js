$(function() {
	$(document).ready(function() {
		//question collapse
		$('.question__title').on("click", function() {
            $(this).toggleClass('active');
			$(this).find($('.question__title-border')).toggleClass('question__title-border-active');
			$(this).closest('.question__tab').find($('.question__text')).toggleClass('question__text-active');
		});
		//question collapse END

		//header menu collapse
		$('.header__collapse').on('click', function(){
			$('.header__nav').toggle("slow");
		});
		//header menu collapse END

		//matchHeight
		$(".ourclients .cards").matchHeight();
		$(".ourclients .cards__title").matchHeight();
		//matchHeight END

		//header__scroll
		$(".header__scroll").on('click', function(event) {
			if (this.hash !== "") {
				event.preventDefault();
				var hash = this.hash;
				$('html, body').animate({
					scrollTop: $(hash).offset().top
				}, 900, function(){
					window.location.hash = hash;
				});
			}
		});
		//header__scroll END

        //inputmask all pages
        $('.input__inputmaskJS').mask("+7(000) 000 - 00 - 00");
        $('.input__inputmaskJS').on('click', function(){
            $(this).val("+7");
        });
        //inputmask all pages END

		//listoption works and filters
		$('.listoption__item').on("click", function() {
			var listData = $(this).html();
			var listValue = $(this).attr('data-filter');
			$(this).closest('.listoption').find($('.listoption__data')).html(listData);
			$(this).closest('.listoption').find($('.listoption__data')).attr('data-filter',listValue);
			$('.geography__img').removeClass('active');
			$('.geography__img').filter('.' + listValue).addClass('active');
		});
		//listoption works and filters END

		//media screen JS
		enquire
			.register("screen and (min-width: 1366px)", {
				match : function() {
					$('.header__rightside').append($('.header__video'));
				},
				unmatch : function() {}
			})
			.register("screen and (min-width: 1024px) and (max-width: 1365px)", {
				match : function() {
					$('.header__rightside').append($('.header__video'));
				},
				unmatch : function() {}
			})
			.register("screen and (min-width: 768px) and (max-width: 1023px)", {
				match : function() {
					$('.header__rightside').append($('.header__video'));
				},
				unmatch : function() {}
			})
			.register("screen and (max-width: 767px)", {
				match : function() {
					$('.header__maintitle').after($('.header__video'));
				},
				unmatch : function() {}
			});
		//media screen JS END

		//clientslider
		var sliderClients = $('.clientslider');
		sliderClients.owlCarousel({
			nav: false,
			dots: false,
			items: 3,
			margin: 10,
			loop: true,
			responsive : {
				0 : {
					items: 2,
					margin: 30
				},
				1024 : {
					items: 3
				}
			}
		});

		$('.ourclients__controls .controls__left').click(function() {
			sliderClients.trigger('prev.owl.carousel');
		});
		$('.ourclients__controls .controls__right').click(function() {
			sliderClients.trigger('next.owl.carousel');
		});
		//clientslider END

		//reviewslider
		var sliderReviews = $('.reviewslider');
		sliderReviews.owlCarousel({
			nav: false,
			dots: true,
			items: 1,
			dotClass: 'reviewslider__dot',
			loop: true,
			dotsContainer: '.reviewslider__dots', //правильный класс !!! задания контейнера с точками
			//navText: [$('.testcontrol__left'),$('.testcontrol__right')]
		});

		$('.reviewslider__controls .controls__left').click(function() {
			sliderReviews.trigger('prev.owl.carousel');
		});
		$('.reviewslider__controls .controls__right').click(function() {
			sliderReviews.trigger('next.owl.carousel');
		});
		//reviewslider END

		//stagesslider
        var sliderStages = $('.stageslider');
        sliderStages.owlCarousel({
			nav: false,
			dots: false,
			items: 2,
			//dotClass: 'reviewslider__dot',
			//dotsClass: 'reviewslider__dots',
			//autoplay: true,
			//autoplayTimeout: 3000,
			//autoplayHoverPause: true,
			loop: true
		});
        $('.howitworks__control-next').click(function() {
            sliderStages.trigger('next.owl.carousel');
        });
        $('.howitworks__control-prev').click(function() {
            sliderStages.trigger('prev.owl.carousel');
        });
		//stagesslider  END

		//sertificate Fancybox
		$("[data-fancybox]").fancybox({});
		//sertificate Fancybox END

		filterSliderItem($('.ourclients__title'), sliderClients);
		filterSliderItem($('.ourclients__links'), sliderClients);
		filterSliderChangeLink($('.ourclients__links-border'), $('.slideresult__link'));
		filterSliderItem($('.videoreview__link'), sliderReviews);

		// function filterSliderItem
		function filterSliderItem(link, slider) {
			//todo refactor to multiple
			var activeLink = link;
			var currentSlider = slider;
			$(activeLink).on("click", function(event) {
				event.preventDefault();
				$(activeLink).removeClass('active');
				$(this).addClass('active');
				var clients = $(this).attr('class').indexOf('ourclients') + 1;
				if (clients) {
					var titleFilter = $('.ourclients__title.active').attr('data-filter');
					var linksFilter = $('.ourclients__links.active').attr('data-filter');
                    var productFilter = '.' + titleFilter + '.' + linksFilter;
                    $('.slideresult__data').hide();
                    $('.slideresult__data[data-filter=' + linksFilter + ']').show();
                } else {
                    productFilter = '.' + $(this).attr('data-filter');
                }
				currentSlider.owlFilter(productFilter);
				var span = $(this).find('span');
				if (checkEvent(span, 'click')) {
					span.trigger('click');
				}
			});
		}
		// function filterSliderItem END

		// function eventsList
		function eventsList(element) {
			// В разных версиях jQuery список событий получается по-разному
			var events = element.data('events');
			if (events !== undefined) return events;

			events = $.data(element, 'events');
			if (events !== undefined) return events;

			events = $._data(element, 'events');
			if (events !== undefined) return events;

			events = $._data(element[0], 'events');
			if (events !== undefined) return events;

			return false;
		}
		// function eventsList END

		// function checkEvent
		function checkEvent(element, eventname) {
			var events,
				ret = false;

			events = eventsList(element);
			if (events) {
				$.each(events, function(evName, e) {
					if (evName == eventname) {
						ret = true;
					}
				});
			}

			return ret;
		}
		// function checkEvent END

		// function filterSliderChangeLink
		function filterSliderChangeLink(link, target) {
			$(link).on("click", function(e) {
				e.stopPropagation();
				var result = $(this).text();
				target.text(result);
			});
		}
		// function filterSliderChangeLink END

		$.fn.owlRemoveItem = function(num) {
			var owl_data = $(this).data('owl.carousel');

			owl_data._items = $.map(owl_data._items, function(data, index) {
				if(index != num) return data;
			});

			$(this).find('.owl-item').eq(num).remove();
		};

		$.fn.owlFilter = function(data, callback) {
			var owl = this,
				$elemCopy = $('<div>').css('display', 'none');

			// check attr owl-clone
			if (typeof($(owl).data('owl-clone')) == 'undefined') {
				$(owl).find('.owl-item:not(.cloned)').clone().appendTo($elemCopy);
				$(owl).data('owl-clone', $elemCopy);
			} else {
				$elemCopy = $(owl).data('owl-clone');
			}
			// check attr owl-clone END

			// clear content
			owl.trigger('replace.owl.carousel', ['<div/>']);

			switch(data){
				case '*':
					$elemCopy.children().each(function() {
						owl.trigger('add.owl.carousel', [$(this).clone()]);
					});
					break;
				default:
					$elemCopy.find(data).each(function() {
						owl.trigger('add.owl.carousel', [$(this).parent().clone()]);
					});
					break;
			}

			//remove item empty when clear
			owl.owlRemoveItem(0);
			owl.trigger('refresh.owl.carousel').trigger('to.owl.carousel', [0]);
			//remove item empty when clear END

			// callback
			if(callback) {
				callback.call(this, owl);
			}
		};

        $('.ourclients__links').eq(0).trigger('click');
		$('.videoreview__link').eq(0).trigger('click');
        $('.ourclients__title').eq(0).trigger('click');
    });
});
