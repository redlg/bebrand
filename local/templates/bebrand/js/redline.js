$(function() {
    $('.cls-1').click(function(e) {
        $('.geography-popup__close').trigger('click');
        const name = $(this).attr('id');
        const region = $(this);
        console.log(name);
        $.ajax({
            method: "POST",
            url: '/ajax/get_region.php',
            data: {'name': name}
        }).done(function(json) {
            if (!json.error) {
                $('#region-id').val(json.ID);
                $('#region-name').val(json.NAME);
                $('#geography-address').text(json.PROPERTY_ADDRESS_VALUE);
                $('#geography-phone').text(json.PROPERTY_PHONE_VALUE);
                $('#geography-work-time').text(json.PROPERTY_WORK_TIME_VALUE);
                const labelHeight = 16;
                const $map = $('#geography__map');
                const $popup = $('#geography-popup');
                const pageX = e.pageX;
                const pageY = e.pageY;
                const clientY = e.clientY;
                var x = pageX - $map.offset().left; //координаты клика относительно обертки карты
                var y = pageY - $map.offset().top;
                if (pageX + $popup.outerWidth() > $(document).width()) {
                    x = x - $popup.outerWidth();
                    $popup.addClass('geography-popup_right-label');
                }
                if (clientY > $popup.outerHeight()) {
                    y = y - $popup.outerHeight() - labelHeight;
                    $popup.addClass('geography-popup_bottom-label');
                } else {
                    y = y + labelHeight;
                }
                region.addClass('cls-1_active');
                $popup.addClass('geography-popup_opened');
                $popup.css({'left': x, 'top': y});
            }
        });

        return false;
    });

    $('.geography-popup__close').click(function() {
        $('.geography-popup').attr('class', 'geography-popup');
        $('.cls-1').removeClass('cls-1_active');
    });

    $('.freecheck__btn, .header__btn, #how-it-works-button, .registration__btn, .services__btn, .callback__text').click(function() {
        $('.overlay').addClass('overlay_opened');
        $('.call-popup').addClass('call-popup_opened');
    });

    $('.call-popup__close, .overlay').click(function() {
        $('.overlay').removeClass('overlay_opened');
        $('.call-popup').removeClass('call-popup_opened');
    });

    $('a[href^="#"]').click(function() {
        var _href = $(this).attr('href');
        $('html, body').animate({scrollTop: $(_href).offset().top + 'px'}, 1000);

        return false;
    });

    $('.questionform__forms').submit(function(e) {
        e.preventDefault();
        var $form = $(this),
            data = $form.serialize(),
            action = $form.attr('action'),
            $button = $('.howitworks__btn-questionform');
        $.ajax({
            method: "POST",
            url: action,
            data: data
        }).done(function(msg) {
            var json = JSON.parse(msg);
            if (json.success) {
                $button.val('Отправлено');
                $button.prop('disabled', true);
            } else {
                alert('Произошла ошибка при отправке. Попробуйте, пожалуйста, поздее');
            }
        });

        return false;
    });

    $('.call-popup__form').submit(function(e) {
        e.preventDefault();
        var $form = $(this),
            data = $form.serialize(),
            action = $form.attr('action'),
            $button = $('.call-popup__btn');
        $.ajax({
            method: "POST",
            url: action,
            data: data
        }).done(function(msg) {
            var json = JSON.parse(msg);
            if (json.success) {
                $button.val('Отправлено');
                $button.prop('disabled', true);
            } else {
                alert('Произошла ошибка при отправке. Попробуйте, пожалуйста, поздее');
            }
        });

        return false;
    });

    $('#geography-form').submit(function(e) {
        e.preventDefault();
        var $form = $(this),
            data = $form.serialize(),
            action = $form.attr('action'),
            $button = $('.geography-popup__btn');
        $.ajax({
            method: "POST",
            url: action,
            data: data
        }).done(function(msg) {
            var json = JSON.parse(msg);
            if (json.success) {
                $button.val('Отправлено');
                $button.prop('disabled', true);
            } else {
                alert('Произошла ошибка при отправке. Попробуйте, пожалуйста, поздее');
            }
        });

        return false;
    });

    const krasnoyarsk = $('#krasnoyarsk');
    var x = krasnoyarsk.offset().left + (krasnoyarsk[0].getBoundingClientRect().width / 2);
    var y = krasnoyarsk.offset().top + (krasnoyarsk[0].getBoundingClientRect().height / 2);
    var e = new $.Event('click');
    e.pageX = x;
    e.pageY = y + 60; //от центра области снижаемся на 60 пикселей, туда где сам город
    krasnoyarsk.trigger(e);
});
