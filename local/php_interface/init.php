<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("iblock");

function pre($arr) {
	global $USER;
	if ($USER->IsAdmin()) {
		echo "<pre>";
		print_r($arr);
		echo "</pre>";
	}
}