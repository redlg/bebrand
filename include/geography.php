<section id="geography" class="container-fluid geography">
	<div class="container geography__block">
		<h2 class="section__title geography__maintitle">География компании</h2>
		<div class="geography__map-wrap">
			<?include 'map.html'?>
			<div id="geography-popup" class="geography-popup">
				<span class="geography-popup__close" title="Закрыть"></span>
				<p class="geography-popup__text" id="geography-address"></p>
				<p class="geography-popup__text" id="geography-work-time"></p>
				<p class="geography-popup__phone" id="geography-phone"></p>
				<form id="geography-form" method="post" action="/ajax/geography.php">
                    <input type="hidden" id="region-id" name="region_id" value="">
                    <input type="hidden" id="region-name" name="region_name" value="">
					<input required name="name" type="text" class="form-control questionform__input geography-popup__input" placeholder="Введите имя">
					<input required name="phone" type="text" class="form-control questionform__input geography-popup__input input__inputmaskJS" placeholder="Введите телефон">
					<input type="submit" class="btn howitworks__btn geography-popup__btn" value="Задать вопрос">
				</form>
			</div>
		</div>
	</div>
</section>
