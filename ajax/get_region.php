<?
require_once ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
header('Content-type: application/json');
CModule::IncludeModule('iblock');

if (isset($_POST['name'])) {
	$arSelect = Array('ID', 'NAME', 'PROPERTY_PHONE', 'PROPERTY_ADDRESS', 'PROPERTY_WORK_TIME');
	$arFilter = Array('IBLOCK_ID' => 9, 'ACTIVE' => 'Y', 'CODE' => $_POST['name']);
	$res = CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
	if($ob = $res->GetNextElement()) {
		echo json_encode($ob->GetFields());
	} else {
		echo json_encode(['error' => 'region not found']);
	}
}