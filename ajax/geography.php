<?
require_once ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');

if (!empty($_POST)) {
	$el = new CIBlockElement;
	$PROP['PHONE'] = $_POST['phone'];
	$PROP['REGION_ID'] = $_POST['region_id'];
	$PROP['REGION_NAME'] = $_POST['region_name'];
	$arLoadProductArray = Array(
		"IBLOCK_SECTION_ID" => false,
		"IBLOCK_ID"         => 10,
		"PROPERTY_VALUES"   => $PROP,
		"NAME"              => $_POST['name'],
		"ACTIVE"            => "Y",
	);
	if ($ID = $el->Add($arLoadProductArray)) {
		$mess = 'Добрый день! На сайте BeBrand новая заявка из блока География компании с номером '.$ID.'<br>';
		$mess .= 'Имя пользователя: ' . $_POST['name'] . '<br>';
		$mess .= 'Телефон: ' . $_POST['phone'] . '<br>';
		$mess .= 'Выбранный регион: ' . $_POST['region_name'] . '<br>';
		$mess .= 'ID выбранного региона: ' . $_POST['region_id'];
		$arEventFields = array(
			"MESSAGE" => $mess,
		);
		CEvent::Send("ORDER_GEOGRAPHY", 's1', $arEventFields);
		$result = array('success' => 1);
	} else {
		$result = array('success' => 0);
	}

	echo json_encode($result);
}