<?
require_once ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule('iblock');

if (!empty($_POST)) {
	$el = new CIBlockElement;
	$PROP['PHONE'] = $_POST['phone'];
	$arLoadProductArray = Array(
		"IBLOCK_SECTION_ID" => false,
		"IBLOCK_ID"         => 5,
		"PROPERTY_VALUES"   => $PROP,
		"NAME"              => $_POST['name'],
		"PREVIEW_TEXT"      => $_POST['question'],
		"ACTIVE"            => "Y",
	);
	if ($ID = $el->Add($arLoadProductArray)) {
		$mess = 'Добрый день! На сайте BeBrand новый вопрос с номером '.$ID.'<br>';
		$mess .= 'Имя пользователя: ' . $_POST['name'] . '<br>';
		$mess .= 'Телефон: ' . $_POST['phone'] . '<br>';
		$mess .= 'Вопрос: ' . $_POST['question'];
		$arEventFields = array(
			"MESSAGE" => $mess,
		);
		CEvent::Send("ORDER_QUESTION", 's1', $arEventFields);
		$result = array('success' => 1);
	} else {
		$result = array('success' => 0);
	}

	echo json_encode($result);
}